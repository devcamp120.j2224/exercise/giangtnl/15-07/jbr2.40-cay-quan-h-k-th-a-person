public class App {
    public static void main(String[] args) throws Exception {
        Person person1 = new Person("Giang", "P. An Phú, Q.2");
        Person person2 = new Person("Linh", "P.13, Q. Bình Thạnh");

        System.out.println( "person1: " + person1.toString() + "\n" +
                            "person2: " + person2.toString());

        Student student1 = new Student("Giang", "P. An Phú, Q.2", "Python", 2022, 20000);
        Student student2 = new Student("Linh", "P.13, Q. Bình Thạnh", "JavaScript", 2022, 30000);

        
        System.out.println( "student1: " + student1.toString() + "\n" +
                            "student2: " + student2.toString());

        Staff staff1 = new Staff("Giang", "P. An Phú, Q.2", "IronHack", 30000) ; 
        Staff staff2 = new Staff("Linh", "P.13, Q. Bình Thạnh", "IronHack", 40000) ; 
        
        System.out.println(  "staff1: " + staff1.toString() + "\n" +
                             "staff2: " + staff2.toString());
    }
}
